package com.growing.blousedesigns.latesstmodels

class CustomException(message: String) : Exception(message)
